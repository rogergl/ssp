package de.ssp.kata.games.standard;

import de.ssp.kata.common.Index;
import de.ssp.kata.common.Randomize;
import de.ssp.kata.common.SymbolPair;

public enum StandardSymbol implements Index {
    STONE(0), SCISSORS(1), PAPER(2);

    private final int index;

    StandardSymbol(int index) {
        this.index = index;
    }

    public static SymbolPair<StandardSymbol> createRandomSymbolPair() {
        Randomize<StandardSymbol> randomize = new Randomize<>(StandardSymbol.values());
        return new SymbolPair<>(
                randomize.randomize(),
                randomize.randomize());
    }

    @Override
    public int getIndex() {
        return index;
    }

}
