package de.ssp.kata.games.standard;

import de.ssp.kata.common.GamePlay;
import de.ssp.kata.common.Score;
import de.ssp.kata.common.ScorePair;
import de.ssp.kata.common.SymbolPair;

public class StandardGame implements GamePlay {

    private final StandardSymbol symbolFirstPlayer;
    private final StandardSymbol symbolSecondPlayer;

    public StandardGame(SymbolPair<StandardSymbol> symbols) {

        if (symbols == null) {
            throw new IllegalArgumentException("symbols must not be null");
        }

        this.symbolFirstPlayer = symbols.getSymbolFirstPlayer();
        this.symbolSecondPlayer = symbols.getSymbolSecondPlayer();
    }

    @Override
    public ScorePair calculateScore() {
        return new ScorePair(rateScore(symbolFirstPlayer, symbolSecondPlayer),
                rateScore(symbolSecondPlayer, symbolFirstPlayer));
    }

    //-------------------------------------------------------------------------
    //                                      Private
    //-------------------------------------------------------------------------

    private static Score rateScore(StandardSymbol symbolA, StandardSymbol symbolB) {

        if (symbolA == symbolB) {
            return Score.DRAW;
        }

        if (((symbolA == StandardSymbol.SCISSORS) && (symbolB == StandardSymbol.PAPER)) ||
                ((symbolA == StandardSymbol.PAPER) && (symbolB == StandardSymbol.STONE)) ||
                ((symbolA == StandardSymbol.STONE) && (symbolB == StandardSymbol.SCISSORS))) {
            return Score.WINNER;
        }

        return Score.LOOSER;

    }
}
