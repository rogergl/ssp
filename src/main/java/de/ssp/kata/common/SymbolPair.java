package de.ssp.kata.common;

import java.util.Random;

public class SymbolPair<T> {

    private final T symbolFirstPlayer;

    private final T symbolSecondPlayer;

    public SymbolPair(T symbolFirstPlayer, T symbolSecondPlayer) {

        if (symbolFirstPlayer == null) {
            throw new IllegalArgumentException("symbolFirstPlayer must not be null");
        }

        if (symbolSecondPlayer == null) {
            throw new IllegalArgumentException("symbolSecondPlayer must not be null");
        }

        this.symbolFirstPlayer = symbolFirstPlayer;
        this.symbolSecondPlayer = symbolSecondPlayer;
    }

    public T getSymbolFirstPlayer() {
        return symbolFirstPlayer;
    }

    public T getSymbolSecondPlayer() {
        return symbolSecondPlayer;
    }

    /**
     * Force that at least one player uses this symbol
     **/
    public SymbolPair<T> forceSymbol(T symbol) {
        T first = symbolFirstPlayer;
        T second = symbolSecondPlayer;

        int playerThatMustUseSymbol = chooseRandomPlayer();
        if (playerThatMustUseSymbol == 1) {
            first = symbol;
        } else {
            second = symbol;
        }

        return new SymbolPair<>(first, second);
    }

    //-------------------------------------------------------------------------
    //                                      Private
    //-------------------------------------------------------------------------

    private static int chooseRandomPlayer() {
        Random rn = new Random();
        return rn.nextInt(2) + 1;
    }
}
