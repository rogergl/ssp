package de.ssp.kata.common;

import java.util.ArrayList;
import java.util.List;

public class Statistics {

    private final List<ScorePair> gameResults;

    public Statistics(List<ScorePair> gameResults) {

        if (gameResults == null) {
            throw new IllegalArgumentException("gameResults must not be null");
        }

        this.gameResults = new ArrayList<>(gameResults);
    }

    public long calculateFirstPlayerWins() {
        return gameResults.stream().filter(s -> s.getScoreFirstPlayer() == Score.WINNER).count();
    }

    public long calculateSecondPlayerWins() {
        return gameResults.stream().filter(s -> s.getScoreSecondPlayer() == Score.WINNER).count();
    }

    public long calculateDraw() {
        return gameResults.stream().filter(s -> s.getScoreFirstPlayer() == Score.DRAW).count();
    }

}
