package de.ssp.kata.common;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Randomize<T extends Index> {

    private final List<T> values;

    public Randomize(T[] values) {
        this.values = Arrays.asList(values);
    }

    /**
     * @return an element from values chosen randomly
     */
    public T randomize() {
        Random rn = new Random();
        int randomInt = rn.nextInt(values.size());
        for (T symbol : values) {
            if (symbol.getIndex() == randomInt) {
                return symbol;
            }
        }
        throw new IllegalStateException("no symbol with index " + randomInt);
    }

}
