package de.ssp.kata.common;

public interface GamePlay {

    ScorePair calculateScore();

}
