package de.ssp.kata.common;

public class ScorePair {

    private final Score scoreFirstPlayer;

    private final Score scoreSecondPlayer;

    public ScorePair(Score scoreFirstPlayer, Score scoreSecondPlayer) {

        if (scoreFirstPlayer == null) {
            throw new IllegalArgumentException("scoreFirstPlayer must not be null");
        }

        if (scoreSecondPlayer == null) {
            throw new IllegalArgumentException("scoreSecondPlayer must not be null");
        }

        this.scoreFirstPlayer = scoreFirstPlayer;
        this.scoreSecondPlayer = scoreSecondPlayer;

        ensureOnlyOne(Score.WINNER);
        ensureOnlyOne(Score.LOOSER);
        ensureBoth(Score.DRAW);

    }

    public Score getScoreFirstPlayer() {
        return scoreFirstPlayer;
    }

    public Score getScoreSecondPlayer() {
        return scoreSecondPlayer;
    }

    //-------------------------------------------------------------------------
    //                                      Private
    //-------------------------------------------------------------------------

    private void ensureOnlyOne(Score score) {
        if ((scoreFirstPlayer == score) && (scoreSecondPlayer == score)) {
            throw new IllegalArgumentException("only one usage possible for " + score);
        }
    }

    private void ensureBoth(Score score) {
        if ((scoreFirstPlayer == score) && (scoreSecondPlayer != score)) {
            throw new IllegalArgumentException("both players must have " + score);
        }
    }

}
