package de.ssp.kata.common;

public enum Score {
    WINNER, LOOSER, DRAW
}
