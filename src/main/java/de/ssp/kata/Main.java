package de.ssp.kata;

import de.ssp.kata.common.ScorePair;
import de.ssp.kata.common.Statistics;
import de.ssp.kata.games.standard.StandardGame;
import de.ssp.kata.games.standard.StandardSymbol;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {

    private Main() {
    }

    public static void main(String[] args) {

        List<ScorePair> gameResults = IntStream.rangeClosed(0, 99)
                .mapToObj(i -> StandardSymbol.createRandomSymbolPair().forceSymbol(StandardSymbol.STONE))
                .map(StandardGame::new)
                .map(StandardGame::calculateScore)
                .collect(Collectors.toList());

        Statistics statistics = new Statistics(gameResults);

        System.out.println(String.format("First player wins %d", statistics.calculateFirstPlayerWins()));
        System.out.println(String.format("Second player wins %d", statistics.calculateSecondPlayerWins()));
        System.out.println(String.format("Draw %d", statistics.calculateDraw()));

    }
}
