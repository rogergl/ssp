package de.ssp.kata.games.standard

import de.ssp.kata.common.Score
import de.ssp.kata.common.SymbolPair
import org.junit.Test

class GameScoreTest {

    @Test
    def void "scissors and paper => scissors wins"() {

        def game1 = new StandardGame(new SymbolPair<>(StandardSymbol.SCISSORS, StandardSymbol.PAPER))
        assert game1.calculateScore().scoreFirstPlayer == Score.WINNER
        assert game1.calculateScore().scoreSecondPlayer == Score.LOOSER


        def game2 = new StandardGame(new SymbolPair<>(StandardSymbol.PAPER, StandardSymbol.SCISSORS))
        assert game2.calculateScore().scoreFirstPlayer == Score.LOOSER
        assert game2.calculateScore().scoreSecondPlayer == Score.WINNER
    }

    @Test
    def void "paper and stone => paper wins"() {

        def game1 = new StandardGame(new SymbolPair<>(StandardSymbol.PAPER, StandardSymbol.STONE))
        assert game1.calculateScore().scoreFirstPlayer == Score.WINNER
        assert game1.calculateScore().scoreSecondPlayer == Score.LOOSER


        def game2 = new StandardGame(new SymbolPair<>(StandardSymbol.STONE, StandardSymbol.PAPER))
        assert game2.calculateScore().scoreFirstPlayer == Score.LOOSER
        assert game2.calculateScore().scoreSecondPlayer == Score.WINNER
    }

    @Test
    def void "stone and scissors => stone wins"() {

        def game1 = new StandardGame(new SymbolPair<>(StandardSymbol.STONE, StandardSymbol.SCISSORS))
        assert game1.calculateScore().scoreFirstPlayer == Score.WINNER
        assert game1.calculateScore().scoreSecondPlayer == Score.LOOSER

        def game2 = new StandardGame(new SymbolPair<>(StandardSymbol.SCISSORS, StandardSymbol.STONE))
        assert game2.calculateScore().scoreFirstPlayer == Score.LOOSER
        assert game2.calculateScore().scoreSecondPlayer == Score.WINNER
    }


    @Test
    def void "same symbols => draw"() {

        def game1 = new StandardGame(new SymbolPair<>(StandardSymbol.STONE, StandardSymbol.STONE))
        assert game1.calculateScore().scoreFirstPlayer == Score.DRAW
        assert game1.calculateScore().scoreSecondPlayer == Score.DRAW

        def game2 = new StandardGame(new SymbolPair<>(StandardSymbol.SCISSORS, StandardSymbol.SCISSORS))
        assert game2.calculateScore().scoreFirstPlayer == Score.DRAW
        assert game2.calculateScore().scoreSecondPlayer == Score.DRAW

        def game3 = new StandardGame(new SymbolPair<>(StandardSymbol.PAPER, StandardSymbol.PAPER))
        assert game3.calculateScore().scoreFirstPlayer == Score.DRAW
        assert game3.calculateScore().scoreSecondPlayer == Score.DRAW

    }
}
