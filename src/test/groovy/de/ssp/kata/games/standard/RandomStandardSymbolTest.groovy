package de.ssp.kata.games.standard

import org.junit.Test

class RandomStandardSymbolTest {

    @Test
    def void "createRandomSymbolPair should return every symbol at least once"() {
        def result = (0..1000).collect { StandardSymbol.createRandomSymbolPair().symbolFirstPlayer }
        assert result.contains(StandardSymbol.PAPER)
        assert result.contains(StandardSymbol.SCISSORS)
        assert result.contains(StandardSymbol.STONE)

        result = (0..1000).collect { StandardSymbol.createRandomSymbolPair().symbolSecondPlayer }
        assert result.contains(StandardSymbol.PAPER)
        assert result.contains(StandardSymbol.SCISSORS)
        assert result.contains(StandardSymbol.STONE)
    }

}
