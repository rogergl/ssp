package de.ssp.kata.common

import org.junit.Test

class StatisticsTest {

    @Test
    def void "should calculate statistics"() {
        def games = [
                new ScorePair(Score.WINNER, Score.LOOSER),
                new ScorePair(Score.WINNER, Score.LOOSER),
                new ScorePair(Score.LOOSER, Score.WINNER),
                new ScorePair(Score.DRAW, Score.DRAW)]
        def stat = new Statistics(games)
        assert stat.calculateFirstPlayerWins() == 2
        assert stat.calculateSecondPlayerWins() == 1
        assert stat.calculateDraw() == 1
    }

}
