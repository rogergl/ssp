package de.ssp.kata.common

import de.ssp.kata.games.standard.StandardSymbol
import org.junit.Test

class SymbolPairTest {

    @Test
    def void "forceSymbol should overwrite symbol"() {
        def symbolToForce = StandardSymbol.STONE
        def pair = new SymbolPair<StandardSymbol>(StandardSymbol.SCISSORS, StandardSymbol.PAPER)
                .forceSymbol(symbolToForce)
        assert pair.symbolFirstPlayer == symbolToForce || pair.symbolSecondPlayer == symbolToForce
    }

}
