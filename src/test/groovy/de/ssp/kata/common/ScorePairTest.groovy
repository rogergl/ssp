package de.ssp.kata.common

import org.junit.Test

class ScorePairTest {

    @Test
    def void "looser and winner should not throw exception"() {
        new ScorePair(Score.LOOSER, Score.WINNER)
        new ScorePair(Score.WINNER, Score.LOOSER)
    }

    @Test
    def void "draw and draw should not throw exception"() {
        new ScorePair(Score.DRAW, Score.DRAW)
    }

    @Test(expected = IllegalArgumentException.class)
    def void "winner and winner should throw exception"() {
        new ScorePair(Score.WINNER, Score.WINNER)
    }

    @Test(expected = IllegalArgumentException.class)
    def void "looser and looser should throw exception"() {
        new ScorePair(Score.LOOSER, Score.LOOSER)
    }

    @Test(expected = IllegalArgumentException.class)
    def void "draw and looser should throw exception"() {
        new ScorePair(Score.DRAW, Score.LOOSER)
        new ScorePair(Score.LOOSER, Score.DRAW)
    }

    @Test(expected = IllegalArgumentException.class)
    def void "draw and winner should throw exception"() {
        new ScorePair(Score.DRAW, Score.WINNER)
        new ScorePair(Score.WINNER, Score.DRAW)
    }

}
