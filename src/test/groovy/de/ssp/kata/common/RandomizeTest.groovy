package de.ssp.kata.common

import de.ssp.kata.games.standard.StandardSymbol
import org.junit.Test

class RandomizeTest {

    @Test
    def void "should return a valid enum"() {
        def randomize = new Randomize<>(StandardSymbol.values())
        def value = randomize.randomize()
        assert value == StandardSymbol.STONE || value == StandardSymbol.SCISSORS ||
                value == StandardSymbol.PAPER
    }

}
