# README #

### This is a simple KATA for stone/scissors/paper ###

* Running tests
  - mvn clean test

* Running the program
  - mvn package
  - java -jar target/ssp-1.0.jar
